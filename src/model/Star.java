package model;

import javax.swing.JTextArea;

public class Star {
	int num;

	public String firstStyle(int num) {
		String loop = "";
		for(int i=1; i<=num-1; i++) {
			for(int j=1; j<=num; j++) {
				loop += "*";
			}
			loop += "\n";
		}return loop;
	}
	
	public String secondStyle(int num) {
		String loop = "";
		for(int i=1; i<=num; i++) {
			for(int j=1; j<=num-1; j++) {
				loop += "*";
			}
			loop += "\n";
		}return loop;
	}
	
	public String thirdStyle(int num) {
		String loop = "";
		for(int i=1; i<=num; i++) {
			for(int j=1; j<=i; j++) {
				loop += "*";
			}
			loop += "\n";
		}return loop;
	}
	
	public String fourthStyle(int num) {
		String loop = "";
		for(int i=1; i<=num; i++) {
			for(int j=1; j<=num+2; j++) {
				if (j % 2 == 0) {
					loop += "*";
				}
				else {
					loop += "_";
				}
			}
			loop += "\n";
		}return loop;
	}
	
	public String fifthStyle(int num) {
		String loop = "";
		for(int i=1; i<=num; i++) {
			for(int j=1; j<=num+2; j++) {
				if ((i + j) % 2 == 0) {
					loop += "*";
				}
				else {
					loop += " ";
				}
			}
			loop += "\n";
		}return loop;
	}
}
