package Control;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import model.Star;
import View.Gui;

public class Controller {

	public Gui gui = new Gui();
	public Star star = new Star();
	
	public Controller(){
		addStarListener();
	}
	
	public static void main(String[] args) {
		new Controller();
	}
	
	public void addStarListener() {
		gui.getButton1().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (gui.getField().getText().isEmpty()) {
					gui.getArea().setText("Please insert size of the star nested loop");
				}
				else {
					int num = Integer.parseInt(gui.getField().getText());
					if (gui.getCombobox().getSelectedItem().toString().equals("Style 1"))
						gui.getArea().setText("NESTED LOOP"+"\n"+star.firstStyle(num));
					else if (gui.getCombobox().getSelectedItem().toString().equals("Style 2"))
						gui.getArea().setText("NESTED LOOP"+"\n"+star.secondStyle(num));
					else if (gui.getCombobox().getSelectedItem().toString().equals("Style 3"))
						gui.getArea().setText("NESTED LOOP"+"\n"+star.thirdStyle(num));
					else if (gui.getCombobox().getSelectedItem().toString().equals("Style 4"))
						gui.getArea().setText("NESTED LOOP"+"\n"+star.fourthStyle(num));
					else 
						gui.getArea().setText("NESTED LOOP"+"\n"+star.fifthStyle(num));
	        }
        }
		});
		gui.getButton2().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gui.getArea().setText("NESTED LOOP");
			}
		});
	}
}
