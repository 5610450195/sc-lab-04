package View;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class Gui extends JFrame {
	
	private JComboBox box;
	
	private JLabel label1;
	private JLabel label2;
	
	private JTextField txt;
	
	private JButton submit;
	private JButton reset;
	
	private JPanel panel;
	private JPanel panel1;
	
	private JTextArea Output;
	
	public Gui()
	{
		createFrame();
	}
	public void createFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(600,450);
		Output = new JTextArea(100,300);
		Output.setText("NESTED LOOP");
		Output.setBackground(Color.LIGHT_GRAY);
		Output.setBorder(new TitledBorder(new EtchedBorder(Color.WHITE, Color.WHITE),(" STAR "),2,1));
		Output.setEditable(false);
		
		panel = new JPanel();
		panel.setLayout(new GridLayout(0, 1));
		
		panel1 = new JPanel();
		panel1.setLayout(null);
		panel1.setBackground(Color.MAGENTA);
		panel1.setBorder(new TitledBorder(new EtchedBorder(Color.WHITE, Color.WHITE),(" INPUT "),2,1));
		
		label1 = new JLabel("SELECT STYLE : ");
		label1.setBounds(80,70,100,22);
		
		label2 = new JLabel("SIZE : ");
		label2.setBounds(137,130,50,22);
		
		box = new JComboBox(new String[] { "Style 1", "Style 2", "Style 3", "Style 4", "Style 5" });
		box.setBounds(180,70,110,22);
		
		txt = new JTextField();
		txt.setBounds(180,130,100,22);
		
		reset = new JButton("Reset");
		reset.setBounds(380,85,100,20);
		
		submit = new JButton("Submit");
		submit.setBounds(380,120,100,20);
		
		add(panel);
		panel1.add(label1);
		panel1.add(label2);
		panel1.add(box);
		panel1.add(txt);
		panel1.add(submit);
		panel1.add(reset);
		
		panel.add(Output);
		panel.add(panel1);

		setVisible(true);
	}
	public JComboBox getCombobox() {
		return box;
	}
	
	public JButton getButton1() {
		return submit;
	}
	
	public JButton getButton2() {
		return reset;
	}

	public JTextArea getArea() {
		return Output;
	}
	
	public JTextField getField() {
		return txt;
	}
}